import  torch
from    torch import nn
from    torchvision import datasets, transforms
from    torch.utils.data import DataLoader
from    torch.nn import functional as F

from    matplotlib import pyplot as plt
import  visdom
from    mconv import MConv2d
import  argparse
import  numpy as np
from    PIL import Image, ImageFilter


class LeNet(nn.Module):
	def __init__(self):
		super(LeNet, self).__init__()
		self.conv1 = nn.Conv2d(3, 3, 5, padding=2)
		self.conv2 = nn.Conv2d(3, 3, 5, padding=2)
		self.conv3 = nn.Conv2d(3, 3, 5, padding=2)

	def forward(self, x):
		# [b, 3, 32, 32]
		# print('x', x.size())

		out = F.relu(self.conv1(x))
		# [6, 14, 14]
		# print('conv1', out.size())

		out = F.relu(self.conv2(out))
		# [16, 5, 5]
		# print('conv2', out.size())
		out = F.relu(self.conv3(out))

		out = F.sigmoid(out)

		return out


class MLeNet(nn.Module):
	def __init__(self):
		super(MLeNet, self).__init__()
		self.conv1 = MConv2d(32, 3, 3, 5, padding=2)
		self.conv2 = MConv2d(32, 3, 3, 5, padding=2)
		self.conv3 = MConv2d(32, 3, 3, 5, padding=2)

	def forward(self, x):
		# print('x', x.size())
		out = F.relu(self.conv1(x))
		# print('conv1', out.size())
		out = F.relu(self.conv2(out))
		# print('conv2', out.size())
		out = F.relu(self.conv3(out))
		out = F.sigmoid(out)
		return out


def main():
	args  = argparse.ArgumentParser()
	args.add_argument('-b', action='store_true', help='set to use basic cnn unit')
	args = args.parse_args()

	batchsz = 20

	db = datasets.CIFAR10('cifar10', train=True, download=True,
	                    transform=transforms.Compose([
		                    transforms.ToTensor(),
		                    # transforms.Normalize((0.5,), (0.5,))
	                    ])
	                    )
	db_loader = DataLoader(db, batch_size=batchsz, shuffle=True)

	db_test = datasets.CIFAR10('cifar10', train=False, download=True,
	                    transform=transforms.Compose([
		                    transforms.ToTensor(),
		                    # transforms.Normalize((0.5,), (0.5,))
	                    ])
	                    )
	db_loader_test = DataLoader(db_test, batch_size=batchsz, shuffle=True)

	device = torch.device('cuda')
	if args.b: # use basic cnn
		lenet = LeNet().to(device)
	else:
		lenet = MLeNet().to(device)
	print(lenet)
	criteon = nn.MSELoss()
	optimizer = torch.optim.Adam(lenet.parameters(), lr=5e-4)

	# interactive plotting
	vis = visdom.Visdom()
	# vis.scatter(torch.tensor([[0,0]]), win='acc cnn',
	# 				            opts={'caption':'acc-cnn','markercolor':np.array([[255,0,0]])})
	# vis.scatter(torch.tensor([[0,0]]), win='acc mcnn',
	# 				            opts={'caption':'acc-cnn','markercolor':np.array([[255,0,0]])})

	for epoch in range(10000):

		for step, batch in enumerate(db_loader):
			# if step * batchsz > 5000:
			# 	break
			# x: [b, 1, 28, 28]
			# y: [10]
			x, _ = batch
			# re-nomalize, [b, 3, h, w] => [b, w, h, 3]
			im = np.multiply(x.numpy(), 255).astype(np.uint8).swapaxes(1, 3)
			# rotate
			im = [Image.fromarray(i, 'RGB') for i in im]
			im = [i.filter(ImageFilter.CONTOUR) for i in im]
			im = np.array([np.array(i) for i in im]).astype(np.float32)
			y = im.swapaxes(1, 3)/255
			# normalize again
			y = torch.from_numpy(y)


			x = x.to(device)
			y = y.to(device)

			pred = lenet(x)

			loss = criteon(pred, y)
			optimizer.zero_grad()
			loss.backward()
			# nn.utils.clip_grad_norm_(lenet.parameters(), 10)
			optimizer.step()


			if step % 5000 == 0:


				# [b, 1, imgsz, imgsz]
				img0 = y.detach()
				img0 = F.upsample(img0, scale_factor=2)
				img1 = pred.detach()
				img1 = F.upsample(img1, scale_factor=2)
				if args.b:
					vis.images(img0, nrow=5, win='cnn-initial image', env='ntm2d')
					vis.images(img1, nrow=5, win='cnn-pred image', env='ntm2d')
				else:
					vis.images(img0, nrow=5, win='mcnn-initial image', env='ntm2d')
					vis.images(img1, nrow=5, win='mcnn-pred image', env='ntm2d')


				print(epoch, step, loss.item())


if __name__ == '__main__':
	main()
