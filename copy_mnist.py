import torch
from torch import nn
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
from torchvision.utils import  make_grid
from ntmcell import NTMCell

from matplotlib import pyplot as plt



def main():

	input_len, input_sz = 28, 28
	output_len, output_sz = 28, 28
	ctrlr_sz = 128
	ctrlr_layer = 3
	N, M = 128, 28
	batchsz = 10

	db = datasets.MNIST('mnist', train=True, download=True,
	                    transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                            ])
	                    )
	db_loader = DataLoader(db, batch_size=batchsz, shuffle=True)

	device = torch.device('cuda')
	ntm = NTMCell(input_sz, output_sz, ctrlr_sz, ctrlr_layer, 1, N, M).to(device)
	criteon = nn.MSELoss()
	optimizer = torch.optim.RMSprop(ntm.parameters(), momentum=0.9, alpha=0.95, lr=1e-4)

	# interactive plotting
	plt.ion()

	for epoch in range(10000):

		for step, batch in enumerate(db_loader):

			# x: [b, 1, 28, 28]
			# y: [10]
			x, _ = batch
			if x.size(0) != batchsz:
				continue
			x = x.to(device)


			# [b, 1, 28, 28] => [b, 28, 28] => [28, b, 28]
			x = x.view(batchsz, 28, 28).transpose(1, 0)
			y = x.clone()

			# fed into NTM
			ntm.zero_state(batchsz)
			for i in range(input_len):
				ntm(x[i])
			pred = torch.zeros(*x.size()).to(device)
			for i in range(output_len):
				pred[i], _ = ntm()

			loss = criteon(pred, y)
			optimizer.zero_grad()
			loss.backward()
			nn.utils.clip_grad_norm_(ntm.parameters(), 10)
			optimizer.step()


			if step % 50 == 0:
				# [28, b, 28]=>[b, 28, 28]=>[b, 1, 28, 28]
				# and restore normalized image data
				img0 = x.transpose(1, 0).unsqueeze(1).mul(0.3081).add(0.1307)
				# [b, 1, 28, 28]=>
				img0 = make_grid(img0.expand(batchsz, 3, 28, 28), nrow=5, padding=10)[0]


				img1 = pred.detach().transpose(1, 0).unsqueeze(1).mul(0.3081).add(0.1307)
				# [b, 1, 28, 28]=>
				img1 = make_grid(img1.expand(batchsz, 3, 28, 28), nrow=5, padding=10)[0]

				plt.subplot(211)
				plt.imshow(img0.cpu().numpy(), cmap='gray')
				plt.subplot(212)
				plt.imshow(img1.cpu().numpy(), cmap='gray')
				plt.draw()
				plt.pause(1e-3)

				print(epoch, step, loss.item())












if __name__ == '__main__':
	main()