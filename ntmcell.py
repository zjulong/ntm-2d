import  torch
from    torch import nn
from    torch.nn import functional as F
from    head import NTMReadHead, NTMWriteHead
from    memory import NTMMemory
import  numpy as np

class NTMCell(nn.Module):

	def __init__(self, input_sz, output_sz, ctrlr_sz, ctrlr_layers, num_heads, N, M):
		"""
		This is a wrapper which can be used as LSTMCell().

		:param input_sz:  input of NTM
		:param output_sz: output of NTM
		:param ctrlr_sz:  hidden units of controller, 128
		:param ctrlr_layers:    layers of hidden of controller, 3
		:param num_heads: heads number, 1 = 1 read and 1 write head
		:param N: memory item numbers, 512
		:param M: memory item size, 32x32
		"""
		super(NTMCell, self).__init__()

		self.input_sz = input_sz
		self.output_sz = output_sz
		self.ctrlr_sz = ctrlr_sz
		self.ctrlr_layers = ctrlr_layers
		self.num_heads = num_heads
		self.N = N
		self.M = M


		# initialize a memory buffer, used by Read/Write Heads.
		# will be automatically added into self module list.
		memory = NTMMemory(N, M)

		# the module added into nn.ModuleList will be included automatically as part of current module.
		heads = nn.ModuleList([])
		self.init_r = []
		for i in range(num_heads):
			heads.extend([
				NTMReadHead(N, M, ctrlr_sz),
				NTMWriteHead(N, M, ctrlr_sz)
			])

			init_r_bias = torch.randn(1, M, M) * 0.01
			# the initial value of read vector is not optimized.
			self.register_buffer("read{}_bias".format(self.num_read_heads), init_r_bias)
			# getattr will return cpu/cuda reference of init_r_bias
			init_r_bias = getattr(self, "read{}_bias".format(self.num_read_heads))
			self.init_r.append(init_r_bias)


		# input dimenson of controller is distinct from input of NTM
		# input + readvector * num
		self.ctrlr_input_sz = input_sz + M*M  * self.num_read_heads
		self.rnn = nn.GRU(input_size=self.ctrlr_input_sz, hidden_size=ctrlr_sz, num_layers=ctrlr_layers)

		# the hidden variable will be optimized by optimizer
		self.lstm_h_bias = nn.Parameter(torch.randn(ctrlr_layers, 1, ctrlr_sz) * 0.05)
		# self.lstm_c_bias = nn.Parameter(torch.randn(ctrlr_layers, 1, ctrlr_sz) * 0.05)

		# output of controller to output of NTM
		# [128 + head_num*20:read vector] => [8]
		self.o2o = nn.Linear(self.ctrlr_sz + self.num_read_heads * M*M, output_sz)


		self.memory = memory
		self.prev_state = None
		self.batchsz = None
		self.reset_parameters()

	def reset(self, batchsz):
		"""
		Initialize the state.
		"""
		self.batchsz = batchsz
		self.memory.reset(batchsz)
		self.prev_state = self.new_state(batchsz)

	def forward(self, x=None):
		"""
		NTM forward function.

		:param x: input vector (batch_size x num_inputs)
		:param prev_state: The previous state of the NTM
		"""
		if x is None: # no input
			# [b, 9]
			x = torch.zeros(self.batchsz, self.input_sz).to(self.o2o.bias.device)

		# unpack the previous state
		# prev_r_list: [[b, M*M], ...]
		# prev_ctrlr_state: [[ctrlr_layers, b, ctrlr_sz], ...]
		# prev_w_list: [[b, N], ...]
		prev_r_list, prev_ctrlr_state, prev_w_list = self.prev_state

		# 1. concat x with read vector to fed into controller
		# [b, 9] concat [b, M*M] on dim=1 => [b, 29]
		inp = torch.cat([x] + prev_r_list, dim=1)

		# 2. fed into controller and get address w
		# [[b, 29], t1] + ([3, b, 128], [3, b,128]) => [[b, 128], t1] + ([3, b, 128], [3, b,128])
		ctrlr_outp, ctrlr_state = self.rnn(inp.unsqueeze(0), prev_ctrlr_state)
		ctrlr_outp = ctrlr_outp.squeeze(0)

		# 3. Read/Write of memory
		r_list = []
		w_list = []
		for head, prev_w in zip(self.heads, prev_w_list):
			# read
			if head.is_read_head():
				r, w = head(ctrlr_outp, prev_w)
				# save historical read
				r_list.append(r)
			# write
			else:
				w = head(ctrlr_outp, prev_w)
			# save historical w
			w_list.append(w)

		# 4. concat output of controller and current read vector to yield output of NTM
		# ctrlr_outp: [b, 128]
		# reads[0]:   [b, 20]
		inp2 = torch.cat([ctrlr_outp] + r_list, dim=1)
		o = F.sigmoid(self.o2o(inp2))


		# x: [b, 9]
		# o: [b, 8]
		# state: (init_r, ctrlr_state, heads_state)
		self.prev_state = (r_list, ctrlr_state, w_list)

		return o, self.prev_state



	def new_state(self, batchsz):
		"""
		Create initial read vector, LSTM status, address w information.
		:param batchsz:
		:return:
		"""
		# initial read vectors, will not be optimized
		# [1, M] => [b, M]
		init_r = [r.clone().repeat(batchsz, 1) for r in self.init_r]
		# initial LSTM hidden variable
		# [layers, b, ctrlr_sz], This initial value will be optimized
		lstm_h = self.lstm_h_bias.clone().repeat(1, batchsz, 1)
		# lstm_c = self.lstm_c_bias.clone().repeat(1, batchsz, 1)
		# initial address w variable: all set to 0
		w_list = [head.new_w(batchsz) for head in self.heads]

		return init_r, lstm_h, w_list



	def reset_parameters(self):
		# initialize O2O network
		nn.init.xavier_uniform_(self.o2o.weight, gain=1)
		nn.init.normal_(self.o2o.bias, std=0.01)
		# initialize LSTM network
		for p in self.rnn.parameters():
			if p.dim() == 1:
				nn.init.constant_(p, 0)
			else:
				stdev = 5 / (np.sqrt(self.ctrlr_input_sz + self.ctrlr_sz))
				nn.init.uniform_(p, -stdev, stdev)