import  torch
import  torch.nn.functional as F
from    torch import nn
import  numpy as np





class NTMMemory(nn.Module):
	"""
	Memory bank for NTM.
	"""

	def __init__(self, N, M):
		"""
		Initialize the NTM Memory matrix.

		The memory's dimensions are (batch size x N x M x M).
		Each batch has it's own memory matrix.

		:param N: Number of N maps.
		:param M: maps with size of MxM.
		"""
		super(NTMMemory, self).__init__()

		self.N = N
		self.M = M

		# NxMxM
		self.register_buffer('mem_bias', torch.Tensor(N, M, M))

		# Initialize memory bias
		stdev = 1 / (np.sqrt(N + M))
		nn.init.uniform_(self.mem_bias, -stdev, stdev)

	def reset(self, batchsz):
		"""
		Initialize memory from bias, for start-of-sequence.
		"""
		self.batchsz = batchsz
		# [b, N, M, M]
		self.memory = self.mem_bias.clone().repeat(batchsz, 1, 1, 1)

	def size(self):
		return self.N, self.M, self.M

	def read(self, w):
		"""
		Read from memory (according to section 3.1).
		"""
		# mem: [b, N, M, M] => [b, M, M, N]
		# w:   [b, N] => [b, 1, N]=> [b, 1, N, 1]
		#      [b, M, M, N] * [b, 1, N, 1]=> [b, M, M, 1] => [b, M, M]
		r = self.memory.permute(0, 2, 3, 1).matmul(w.unsqueeze(1).unsqueeze(1)).squeeze(3)
		return r

	def write(self, w, e, a):
		"""
		write to memory (according to section 3.2).
		:param w: [b, N]
		:param e: [b, M, M]
		:param a: [b, M, M]
		"""
		self.prev_mem = self.memory
		# [b, N, M, M]
		# TODO: CUDA?
		self.memory = torch.Tensor(self.batchsz, self.N, self.M, self.M).to(self.prev_mem.device)
		# [b, N] => [b, N, 1]
		# [b, M, M] => [b, M*M] => [b, 1, M*M]
		# [b, N, 1] * [b, 1, M*M] => [b, N, M*M]
		erase = torch.matmul(w.unsqueeze(2), e.view(e.size(0), -1).unsqueeze(1))
		# [b, N, M*M] => [b, N, M, M]
		erase = erase.view(e.size(0), self.N, self.M, self.M)


		# [b, N] => [b, N, 1]
		# [b, M, M] => [b, M*M] => [b, 1, M*M]
		# [b, N, 1] * [b, 1, M*M] => [b, N, M*M]
		add = torch.matmul(w.unsqueeze(2), a.view(a.size(0), -1).unsqueeze(1))
		# [b, N, M*M] => [b, N, M, M]
		add = add.view(a.size(0), self.N, self.M, self.M)

		# update memory
		self.memory = self.prev_mem * (1 - erase) + add

	def address(self, k, beta, g, s, gamma, w_prev):
		"""
		NTM Addressing (according to section 3.3).

		Returns a softmax weighting over the rows of the memory matrix.

		:param k: [b, N]
		:param beta:
		:param g:
		:param s:
		:param gamma:
		:param w_prev: [b, N]
		"""
		# activation for each
		k = k.clone()                   # [b, N]
		beta = F.softplus(beta)         # [b, 1]
		g = F.sigmoid(g)                # [b, 1]
		s = F.softmax(s, dim=1)         # [b, 3]
		gamma = 1 + F.softplus(gamma)   # [b, 1]

		# Content addressing
		# [b, M, M], scalar => [b, N]
		wc = self._similarity(k, beta)

		# Location addressing
		# [b, N], [b, N], scalar => [b, N]
		wg = self._interpolate(w_prev, wc, g)

		# [b, N], [b, 3] => [b, N]
		w_wave = self._shift(wg, s)

		w = self._sharpen(w_wave, gamma)

		return w

	def _convolve(self, w, s):
		"""
		Circular convolution implementation.
		:param w:  [b, N]=> [N]
		:param s:  [b, 3]=> [3]
		:return:
		"""
		assert s.size(0) == 3  # w: [128], s[3]

		t = torch.cat([w[-1:], w, w[:1]])  # [128+2]
		# t: [130] => [1, 1, 130]
		# s: [3] => [1, 1, 3]
		# conv1d: [1, 1, 128]=> [128]
		c = F.conv1d(t.view(1, 1, -1), s.view(1, 1, -1)).view(-1)  # [128]
		return c

	def _similarity(self, k, beta):
		"""

		:param k: [b, M, M]
		:param beta:  [b, 1]
		:return:
		"""
		# k: [b, M, M] => [b, 1, M*M]
		k = k.view(self.batchsz, 1, self.M * self.M)
		# memory: [b, N, M, M] => [b, N, M*M]
		# similarity:  [b, N, M*M] vs [b, 1, M*M] => [b, N]
		# softmax: [b, 1] * [b, N] => [b, N] softmax=> [b, N]
		w = F.softmax(beta * F.cosine_similarity(self.memory.view(self.batchsz, self.N, -1), k, dim=2, eps=1e-16), dim=1)

		return w

	def _interpolate(self, w_prev, wc, g):
		"""

		:param w_prev:  [b, N]
		:param wc:      [b, N]
		:param g:       [b, 1]
		:return:        [b, N]
		"""
		return g * wc + (1 - g) * w_prev

	def _shift(self, wg, s):
		"""

		:param wg:  [b, N]
		:param s:   [b, 3]
		:return:    [b, N]
		"""
		# [b, N]
		result = torch.zeros(wg.size()).to(self.memory.device)

		for b in range(self.batchsz):
			# [N], [3] => [N]
			result[b] = self._convolve(wg[b], s[b])
		return result

	def _sharpen(self, w_wave, gamma):
		"""

		:param w_wave:  [b, N]
		:param gamma:   [b, 1]
		:return:        [b, N]
		"""
		# [b, N]
		w = w_wave ** gamma
		# [b, N] sum=> [b] => [b, 1]
		# div: [b, N] / [b, 1] => [b, N]
		w = torch.div(w, torch.sum(w, dim=1).view(-1, 1) + 1e-16)
		return w

	def __repr__(self):
		return 'NTMMemory(N:%d, M:%d, mem_bias:(%d,%d,%d))'%(self.N, self.M, self.N, self.M, self.M)