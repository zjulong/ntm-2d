# NTM-Pytorch
Neural Turing Machine or Differentiable Neural Computer Implementation in Pytorch
Support CUDA accerleration.

# Platform
- python: 3.x
- pytorch: 0.4+


# HOWTO

`python copy_task.py`

# Screenshot
![run](res/run.png)
