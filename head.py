import  torch
from    torch import nn
import  torch.nn.functional as F
import  numpy as np



class NTMReadHead(nn.Module):

	def __init__(self, N, M, ctrlr_sz):
		"""

		:param memory:
		:param ctrlr_sz: hidden layer size or output size of RNN
		"""
		super(NTMReadHead, self).__init__()

		self.N, self.M = N, M
		self.ctrlr_sz = ctrlr_sz

		# Corresponding to k,           beta, g, s, gamma sizes from the paper
		# k is of size of M, and s unit of size 3
		self.read_len = [self.M * self.M, 1, 1, 3, 1]

		# hidden_sz => variables length
		self.fc_read = nn.Linear(ctrlr_sz, sum(self.read_len))

		self.reset_parameters()

	def new_w(self, batchsz):
		# The state holds the previous time step address weightings
		# [b, N]
		return torch.zeros(batchsz, self.N).to(self.fc_read.bias.device)

	def reset_parameters(self):
		# Initialize the linear layers
		nn.init.xavier_uniform_(self.fc_read.weight, gain=1.4)
		nn.init.normal_(self.fc_read.bias, std=0.01)

	def is_read_head(self):
		return True

	def forward(self, h, w_prev):
		"""
		NTMReadHead forward function.

		:param h: [b, ctrlr_sz] controller hidden variable
		:param w_prev: [b, N] previous step state
		"""
		# [b, ctrlr_sz] => [b, vars_len]
		o = self.fc_read(h)
		# [b, 26] split with [20, 1, 1, 3, 1]
		k, beta, g, s, gamma = torch.split(o, self.read_len, dim=1)
		# [b, M*M] => [b, M, M]
		k = k.view(k.size(0), self.M, self.M)

		# # obtain address w
		# w = self.memory.address(k, beta, g, s, gamma, w_prev)
		# # read
		# r = self.memory.read(w)

		return k, beta, g, s, gamma, w_prev




class NTMWriteHead(nn.Module):

	def __init__(self, N, M, ctrlrsz):
		"""

		:param memory:
		:param ctrlrsz:
		"""
		super(NTMWriteHead, self).__init__()

		self.N, self.M = N, M
		self.ctrlrsz = ctrlrsz

		# Corresponding to        k,    beta, g, s, gamma, e,           a sizes from the paper
		self.write_len = [self.M*self.M, 1, 1,   3, 1,   self.M*self.M, self.M*self.M]
		# [b, ctrlrsz] => [b, vars_len]
		self.fc_write = nn.Linear(ctrlrsz, sum(self.write_len))
		self.reset_parameters()

	def new_w(self, batch_size):
		return torch.zeros(batch_size, self.N).to(self.fc_write.bias.device)

	def reset_parameters(self):
		# Initialize the linear layers
		nn.init.xavier_uniform_(self.fc_write.weight, gain=1.4)
		nn.init.normal_(self.fc_write.bias, std=0.01)

	def is_read_head(self):
		return False

	def forward(self, h, w_prev):
		"""
		NTMWriteHead forward function.

		:param h: [b, ctrlr] controller hidden variable
		:param w_prev: [b, N] previous step state
		"""
		# [b, ctrlr_sz] => [b, vars_sum]
		o = self.fc_write(h)
		# [b, len] split with [20, 1, 1, 3, 1, 20, 20]
		k, beta, g, s, gamma, e, a = torch.split(o, self.write_len, dim=1)
		k = k.view(k.size(0), self.M, self.M)
		e = e.view(e.size(0), self.M, self.M)
		a = a.view(a.size(0), self.M, self.M)

		# e should be in [0, 1]
		e = F.sigmoid(e)

		# # retain address w
		# w = self.memory.address(k, beta, g, s, gamma, w_prev)
		# # write into memory
		# self.memory.write(w, e, a)

		return (k, beta, g, s, gamma, w_prev), e, a


