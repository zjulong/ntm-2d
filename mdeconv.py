import  torch
from    torch import nn
from    torch.nn import functional as F
import  numpy as np


class CNNMemory(nn.Module):
	"""
	Memory bank for CNN.
	"""

	def __init__(self, N, M):
		"""
		Initialize the CNN Memory database.

		The memory's dimensions are (N x M x M).

		:param N: Number of N maps.
		:param M: maps with size of MxM.
		"""
		super(CNNMemory, self).__init__()

		self.N = N
		self.M = M

		# [N, M, M]
		self.register_buffer('memory', torch.Tensor(N, M, M))

		# Initialize memory
		stdev = 1 / (np.sqrt(N + M))
		# nn.init.uniform_(self.memory, -stdev, stdev)
		nn.init.constant_(self.memory, 0)

		# w1 = nn.Parameter(torch.Tensor())


	def read(self, w):
		"""
		Read from memory
		:param w: [b, C, N]
		:return : [b, C, M, M]
		"""
		batchsz, C, _ = w.size()
		# w:   [b, C, N]
		#      [b, C, N] * [N, M*M] => [b, C, M*M]
		r = w.matmul(self.memory.view(self.N , self.M * self.M ))
		# [b, C, M*M] => [b, C, M, M]
		r = r.view(batchsz, C, self.M, self.M)

		return r

	def write(self, w, f):
		"""
		Write to memory
		M = M0 + learning_rate * (Feature_map - M0)
		:param w: [b, C, N]
		:param f: [b, C, M, M], C is the output channel of conv2d
		:return read: [b, C, M, M]
		"""
		batchsz, C, _ = w.size()
		# [b, C, M, M]
		r = self.read(w)
		# [b, C, M, M] - [b, C, M, M]
		delta = 1e-5 * (f - r)
		# [b, C, M, M] => [b*C, M*M]
		delta = delta.view(batchsz * C, self.M * self.M)
		# [b, C, N] => [b*c, N] => [N, b*c]
		w = w.view(batchsz * C, self.N).transpose(0, 1)
		# [N, b*c] * [b*c, M*M] => [N, M*M] => [N, M, M]
		delta = torch.matmul(w, delta).view(self.N, self.M, self.M)
		# [N, M, M]
		self.memory = self.memory + delta.detach()

		return r

	def address(self, f):
		"""
		Content addressing only.
		Returns a softmax weighting over the rows of the memory matrix.
		:param f:  [b, C, M, M]
		:return w: [b, C, N]
		"""

		# Content addressing
		batchsz, C, M, M = f.size()
		# [b, C, M, M] => [b*c, 1, M*M]
		f = f.view(batchsz * C, 1, M * M)
		# memory: [N, M*M] => [1, N, M*M]
		# similarity: [b*c, 1, M*M] with [1, N, M*M] => [b*c, N]
		similarity = F.cosine_similarity(f, self.memory.view(self.N, M*M).unsqueeze(0), dim=2, eps=1e-16)
		# [b*c, N] => [b, C, N]
		similarity = similarity.view(batchsz, C, self.N)
		# [b, C, N]
		w = F.softmax(similarity, dim=2)

		return w


	def __repr__(self):
		return 'CNNMemory(N:%d, M:%d, memory:(%d,%d,%d))'%(self.N, self.M, self.N, self.M, self.M)




class MDeConv2d(nn.Module):
	"""
	Memory DeConvolution Neural Module
	"""

	def __init__(self, imgsz, inpch, outpch, ksz, stride=1, padding=0):
		super(MDeConv2d, self).__init__()

		self.conv = nn.ConvTranspose2d(inpch, outpch, kernel_size=ksz, stride=stride, padding=padding)

		outp = self.conv(torch.randn(3, inpch, imgsz, imgsz))
		print('MDeConv2d outp sz:', outp.shape)
		# [3, C, M, M]
		_, self.C, self.M, _ = outp.size()
		self.N  = 10

		# [N, M, M]
		self.memory = CNNMemory(self.N, self.M)




	def forward(self, x):
		"""
		x: [b, inpch, imgsz, imgsz] => f: [b, C, M, M]
		=> f_mem: [b, C, M, M]
		:param x: [b, inpch, imgsz, imgsz]
		:return:
		"""
		# convolution output
		# [b, C, M, M]
		f = self.conv(x)

		# [b, C, N]
		w = self.memory.address(f)
		# read: [b, C, M, M]
		r = self.memory.write(w, f)
		# aggregate [b, C, M, M]
		f_mem = (f + r )/ 2

		return f_mem



