import  os, time, argparse
import  visdom
import  itertools
import  pickle
import  imageio

import  torch
import  torch.nn as nn
import  torch.nn.functional as F
import  torch.optim as optim
from    torchvision import datasets, transforms
import  numpy as np
import  matplotlib
matplotlib.use('agg')
from    matplotlib import pyplot as plt

from    mdeconv import MDeConv2d
from    mconv import MConv2d

def normal_init(m, mean, std):
	if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
		m.weight.data.normal_(mean, std)
		m.bias.data.zero_()

class MGenerator(nn.Module):

	def __init__(self, d=128):
		super(MGenerator, self).__init__()

		#        in ch, out ch, kernel, stride, padding
		self.deconv1 = MDeConv2d(1, 100, d * 8, 4, 1, 0)
		self.deconv1_bn = nn.BatchNorm2d(d * 8)
		self.deconv2 = MDeConv2d(4, d * 8, d * 4, 4, 2, 1)
		self.deconv2_bn = nn.BatchNorm2d(d * 4)
		self.deconv3 = MDeConv2d(8, d * 4, d * 2, 4, 2, 1)
		self.deconv3_bn = nn.BatchNorm2d(d * 2)
		self.deconv4 = MDeConv2d(16, d * 2, d, 4, 2, 1)
		self.deconv4_bn = nn.BatchNorm2d(d)
		self.deconv5 = MDeConv2d(32, d, 3, 4, 2, 1)

	# weight_init
	def weight_init(self, mean, std):
		for m in self._modules:
			normal_init(self._modules[m], mean, std)

	# forward method
	def forward(self, input):
		"""
		x torch.Size([128, 100, 1, 1])
		deconv1 torch.Size([128, 1024, 4, 4])
		deconv2 torch.Size([128, 512, 8, 8])
		deconv3 torch.Size([128, 256, 16, 16])
		deconv4 torch.Size([128, 128, 32, 32])
		deconv5 torch.Size([128, 1, 64, 64])

		"""
		# print('x', input.shape)
		x = F.relu(self.deconv1_bn(self.deconv1(input)))
		# print('deconv1', x.shape)
		x = F.relu(self.deconv2_bn(self.deconv2(x)))
		# print('deconv2', x.shape)
		x = F.relu(self.deconv3_bn(self.deconv3(x)))
		# print('deconv3', x.shape)
		x = F.relu(self.deconv4_bn(self.deconv4(x)))
		# print('deconv4', x.shape)
		x = F.tanh(self.deconv5(x))
		# print('deconv5', x.shape)

		return x

class Generator(nn.Module):

	def __init__(self, d=128):
		super(Generator, self).__init__()

		#        in ch, out ch, kernel, stride, padding
		self.deconv1 = nn.ConvTranspose2d(100, d * 8, 4, 1, 0)
		self.deconv1_bn = nn.BatchNorm2d(d * 8)
		self.deconv2 = nn.ConvTranspose2d(d * 8, d * 4, 4, 2, 1)
		self.deconv2_bn = nn.BatchNorm2d(d * 4)
		self.deconv3 = nn.ConvTranspose2d(d * 4, d * 2, 4, 2, 1)
		self.deconv3_bn = nn.BatchNorm2d(d * 2)
		self.deconv4 = nn.ConvTranspose2d(d * 2, d, 4, 2, 1)
		self.deconv4_bn = nn.BatchNorm2d(d)
		self.deconv5 = nn.ConvTranspose2d(d, 3, 4, 2, 1)

	# weight_init
	def weight_init(self, mean, std):
		for m in self._modules:
			normal_init(self._modules[m], mean, std)

	# forward method
	def forward(self, input):
		"""
		x torch.Size([128, 100, 1, 1])
		deconv1 torch.Size([128, 1024, 4, 4])
		deconv2 torch.Size([128, 512, 8, 8])
		deconv3 torch.Size([128, 256, 16, 16])
		deconv4 torch.Size([128, 128, 32, 32])
		deconv5 torch.Size([128, 1, 64, 64])

		"""
		# print('x', input.shape)
		x = F.relu(self.deconv1_bn(self.deconv1(input)))
		# print('deconv1', x.shape)
		x = F.relu(self.deconv2_bn(self.deconv2(x)))
		# print('deconv2', x.shape)
		x = F.relu(self.deconv3_bn(self.deconv3(x)))
		# print('deconv3', x.shape)
		x = F.relu(self.deconv4_bn(self.deconv4(x)))
		# print('deconv4', x.shape)
		x = F.tanh(self.deconv5(x))
		# print('deconv5', x.shape)

		return x



class MDiscriminator(nn.Module):

	def __init__(self, d=128):
		super(MDiscriminator, self).__init__()
		"""
		x torch.Size([128, 1, 64, 64])
		conv1 torch.Size([128, 128, 32, 32])
		conv2 torch.Size([128, 256, 16, 16])
		conv3 torch.Size([128, 512, 8, 8])
		conv4 torch.Size([128, 1024, 4, 4])
		conv5 torch.Size([128, 1, 1, 1])

		"""
		self.conv1 = MConv2d(64, 3, d, 4, 2, 1)
		self.conv2 = MConv2d(32, d, d * 2, 4, 2, 1)
		self.conv2_bn = nn.BatchNorm2d(d * 2)
		self.conv3 = MConv2d(16, d * 2, d * 4, 4, 2, 1)
		self.conv3_bn = nn.BatchNorm2d(d * 4)
		self.conv4 = MConv2d(8, d * 4, d * 8, 4, 2, 1)
		self.conv4_bn = nn.BatchNorm2d(d * 8)
		self.conv5 = MConv2d(4, d * 8, 1, 4, 1, 0)

	# weight_init
	def weight_init(self, mean, std):
		for m in self._modules:
			normal_init(self._modules[m], mean, std)

	# forward method
	def forward(self, input):
		# print('x', input.shape)
		x = F.leaky_relu(self.conv1(input), 0.2)
		# print('conv1', x.shape)
		x = F.leaky_relu(self.conv2_bn(self.conv2(x)), 0.2)
		# print('conv2', x.shape)
		x = F.leaky_relu(self.conv3_bn(self.conv3(x)), 0.2)
		# print('conv3', x.shape)
		x = F.leaky_relu(self.conv4_bn(self.conv4(x)), 0.2)
		# print('conv4', x.shape)
		x = F.sigmoid(self.conv5(x))
		# print('conv5', x.shape)

		return x

class Discriminator(nn.Module):

	def __init__(self, d=128):
		super(Discriminator, self).__init__()
		"""
		x torch.Size([128, 1, 64, 64])
		conv1 torch.Size([128, 128, 32, 32])
		conv2 torch.Size([128, 256, 16, 16])
		conv3 torch.Size([128, 512, 8, 8])
		conv4 torch.Size([128, 1024, 4, 4])
		conv5 torch.Size([128, 1, 1, 1])

		"""
		self.conv1 = nn.Conv2d(3, d, 4, 2, 1)
		self.conv2 = nn.Conv2d(d, d * 2, 4, 2, 1)
		self.conv2_bn = nn.BatchNorm2d(d * 2)
		self.conv3 = nn.Conv2d(d * 2, d * 4, 4, 2, 1)
		self.conv3_bn = nn.BatchNorm2d(d * 4)
		self.conv4 = nn.Conv2d(d * 4, d * 8, 4, 2, 1)
		self.conv4_bn = nn.BatchNorm2d(d * 8)
		self.conv5 = nn.Conv2d(d * 8, 1, 4, 1, 0)

	# weight_init
	def weight_init(self, mean, std):
		for m in self._modules:
			normal_init(self._modules[m], mean, std)

	# forward method
	def forward(self, input):
		# print('x', input.shape)
		x = F.leaky_relu(self.conv1(input), 0.2)
		# print('conv1', x.shape)
		x = F.leaky_relu(self.conv2_bn(self.conv2(x)), 0.2)
		# print('conv2', x.shape)
		x = F.leaky_relu(self.conv3_bn(self.conv3(x)), 0.2)
		# print('conv3', x.shape)
		x = F.leaky_relu(self.conv4_bn(self.conv4(x)), 0.2)
		# print('conv4', x.shape)
		x = F.sigmoid(self.conv5(x))
		# print('conv5', x.shape)

		return x





fixed_z_ = torch.randn((5 * 5, 100)).view(-1, 100, 1, 1).cuda()  # fixed noise


def show_result(num_epoch, path='result.png', isFix=False):
	z_ = torch.randn((5 * 5, 100)).view(-1, 100, 1, 1).cuda()

	G.eval()
	if isFix:
		test_images = G(fixed_z_)
	else:
		test_images = G(z_)
	G.train()

	test_images = test_images.mul(0.5).add(0.5)

	size_figure_grid = 5
	fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(5, 5))
	for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
		ax[i, j].get_xaxis().set_visible(False)
		ax[i, j].get_yaxis().set_visible(False)

	# print('test_images', test_images.shape)
	for k in range(5 * 5):
		i = k // 5
		j = k % 5
		ax[i, j].cla()
		tmp = test_images[k].detach().permute(1,2,0).cpu().numpy()
		ax[i, j].imshow(tmp)

	label = 'Epoch {0}'.format(num_epoch)
	fig.text(0.5, 0.04, label, ha='center')
	plt.savefig(path)
	plt.close()

	win = 'isFix-mini' if isFix else 'random-mini'
	win =  ('Deconv-' + win) if args.b else ('MDeconv-' + win)
	vis.images(test_images.detach(), nrow=5, win=win, opts=dict(title=win))


def show_train_hist(hist, path='Train_hist.png'):
	x = range(len(hist['D_losses']))

	y1 = hist['D_losses']
	y2 = hist['G_losses']

	plt.plot(x, y1, label='D_loss')
	plt.plot(x, y2, label='G_loss')

	plt.xlabel('Iter')
	plt.ylabel('Loss')

	plt.legend(loc=4)
	plt.grid(True)
	plt.tight_layout()

	plt.savefig(path)
	plt.close()


batch_size = 128
lr = 0.0002
train_epoch = 5000
img_size = 64
transform = transforms.Compose([
	transforms.Resize((img_size, img_size)),
	transforms.ToTensor(),
	transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
])
# train_loader = torch.utils.data.DataLoader(
# 	datasets.CIFAR10('cifar10', train=True, download=True, transform=transform),
# 	batch_size=batch_size, shuffle=True)
db = datasets.ImageFolder('/hdd1/liangqu/datasets/miniimagenet/train/',
                          transform=transforms.Compose([
	                          transforms.Resize((64, 64)),
	                          transforms.ToTensor(),
	                          # transforms.Normalize((0.5,), (0.5,))
                          ]))
train_loader = torch.utils.data.DataLoader(db, batch_size=batch_size, shuffle=True)


args = argparse.ArgumentParser()
args.add_argument('-b', action='store_true', help='set to use basic decnn unit')
args = args.parse_args()

vis = visdom.Visdom()
if args.b:
	G = Generator(128).cuda()
	D = Discriminator(128).cuda()
else:
	G = MGenerator(128).cuda()
	D = Discriminator(128).cuda()

print(G)
print(D)
G.weight_init(mean=0.0, std=0.02)
D.weight_init(mean=0.0, std=0.02)


BCE_loss = nn.BCELoss()
G_optimizer = optim.Adam(G.parameters(), lr=lr, betas=(0.5, 0.999))
D_optimizer = optim.Adam(D.parameters(), lr=lr, betas=(0.5, 0.999))

if args.b:
	print('DeConv2d training start!')
	path = 'MINI_DCGAN'
else:
	print(' MDeConv2d training start!')
	path = 'MINI_MDCGAN'

# results save folder
if not os.path.isdir(path):
	os.mkdir(path)
if not os.path.isdir(path + '/Random_results'):
	os.mkdir(path+'/Random_results')
if not os.path.isdir(path+'/Fixed_results'):
	os.mkdir(path+'/Fixed_results')

train_hist = {}
train_hist['D_losses'] = []
train_hist['G_losses'] = []
train_hist['per_epoch_ptimes'] = []
train_hist['total_ptime'] = []
num_iter = 0



start_time = time.time()
for epoch in range(train_epoch):
	D_losses = []
	G_losses = []
	epoch_start_time = time.time()
	for step, (x_, _) in enumerate(train_loader):

		mini_batch = x_.size()[0]
		y_real_ = torch.ones(mini_batch)
		y_fake_ = torch.zeros(mini_batch)

		# real loss
		x_, y_real_, y_fake_ = x_.cuda(), y_real_.cuda(), y_fake_.cuda()
		D_result = D(x_).squeeze()
		D_real_loss = BCE_loss(D_result, y_real_)

		# fake loss
		z_ = torch.randn((mini_batch, 100)).view(-1, 100, 1, 1).cuda()
		G_result = G(z_)
		D_result = D(G_result).squeeze()
		D_fake_loss = BCE_loss(D_result, y_fake_)


		D_train_loss = D_real_loss + D_fake_loss
		D.zero_grad()
		D_train_loss.backward()
		D_optimizer.step()
		D_losses.append(D_train_loss.item())



		# train G
		z_ = torch.randn((mini_batch, 100)).view(-1, 100, 1, 1).cuda()
		G_result = G(z_)
		D_result = D(G_result).squeeze()
		G_train_loss = BCE_loss(D_result, y_real_)
		G.zero_grad()
		G_train_loss.backward()
		G_optimizer.step()
		G_losses.append(G_train_loss.item())

		num_iter += 1

		if step % 500 ==0:
			epoch_end_time = time.time()
			per_epoch_ptime = epoch_end_time - epoch_start_time

			print('[%d/%d] - ptime: %.2f, loss_d: %.3f, loss_g: %.3f' % (
			(epoch + 1), train_epoch, per_epoch_ptime, np.array(D_losses).mean(), np.array(G_losses).mean()))

			p = path+'/Random_results/CIFAR10_DCGAN_' + str(epoch + 1) + '.png'
			fixed_p = path+'/Fixed_results/CIFAR10_DCGAN_' + str(epoch + 1) + '.png'
			show_result((epoch + 1), path=p, isFix=False)
			show_result((epoch + 1), path=fixed_p, isFix=True)
			train_hist['D_losses'].append(np.array(D_losses).mean())
			train_hist['G_losses'].append(np.array(G_losses).mean())
			train_hist['per_epoch_ptimes'].append(per_epoch_ptime)

			epoch_start_time = time.time()

end_time = time.time()
total_ptime = end_time - start_time
train_hist['total_ptime'].append(total_ptime)

print("Avg per epoch ptime: %.2f, total %d epochs ptime: %.2f" % (
torch.mean(torch.FloatTensor(train_hist['per_epoch_ptimes'])), train_epoch, total_ptime))
print("Training finish!... save training results")
torch.save(G.state_dict(), path + "/generator_param.pkl")
torch.save(D.state_dict(), path + "/discriminator_param.pkl")
with open(path + '/train_hist.pkl', 'wb') as f:
	pickle.dump(train_hist, f)

name = path+'/DeConv_hist.png' if args.b else path+'/MDeConv_hist.png'
show_train_hist(train_hist, path=name)


# make gif image
images = []
for e in range(train_epoch):
	img_name = path+'/Fixed_results/CIFAR10_DCGAN_' + str(e + 1) + '.png'
	images.append(imageio.imread(img_name))
name = path+'/DeConv.gif' if args.b else path+'/MDeConv.gif'
imageio.mimsave(name, images, fps=5)
