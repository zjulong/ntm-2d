import torch
from torch import nn
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
from torchvision.utils import  make_grid
from NTMCell import NTMCell

from matplotlib import pyplot as plt
import visdom


def main():

	imgsz = 64
	input_len, input_sz = imgsz, imgsz
	output_len, output_sz = imgsz, imgsz
	ctrlr_sz = 256
	ctrlr_layer = 3
	N, M = 256, imgsz
	batchsz = 10

	db = datasets.CIFAR100('cifar100', train=True, download=True,
	                    transform=transforms.Compose([
		                    transforms.Grayscale(),
		                    transforms.Resize((imgsz,imgsz) ),
                            transforms.ToTensor(),
                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                            ])
	                    )
	db_loader = DataLoader(db, batch_size=batchsz, shuffle=True)

	device = torch.device('cuda')
	ntm = NTMCell(input_sz, output_sz, ctrlr_sz, ctrlr_layer, 1, N, M).to(device)
	criteon = nn.MSELoss()
	optimizer = torch.optim.RMSprop(ntm.parameters(), momentum=0.9, alpha=0.95, lr=1e-4)

	# interactive plotting
	plt.ion()
	vis = visdom.Visdom()

	for epoch in range(10000):

		for step, batch in enumerate(db_loader):

			# x: [b, 1, imgsz, imgsz]
			# y: [10]
			x, _ = batch
			if x.size(0) != batchsz:
				continue
			x = x.to(device)


			# [b, 1, imgsz, imgsz] => [b, imgsz, imgsz] => [imgsz, b, imgsz]
			x = x.squeeze(1).transpose(0, 1)
			y = x.clone()

			# fed into NTM
			ntm.zero_state(batchsz)
			for i in range(input_len):
				ntm(x[i])
			pred = torch.zeros(*x.size()).to(device)
			for i in range(output_len):
				pred[i], _ = ntm()

			loss = criteon(pred, y)
			optimizer.zero_grad()
			loss.backward()
			nn.utils.clip_grad_norm_(ntm.parameters(), 10)
			optimizer.step()


			if step % 40 == 0:
				# [b, imgsz, imgsz]
				img0 = x.transpose(0, 1).unsqueeze(1).mul(0.5).add(0.5)
				img1 = pred.detach().transpose(0, 1).unsqueeze(1).mul(0.5).add(0.5)
				vis.images(img0, nrow=5, win='initial image')
				vis.images(img1, nrow=5, win='memory image')

				print(epoch, step, loss.item())












if __name__ == '__main__':
	main()